import { useState, useEffect, useContext } from 'react';
import Card from '../Cards/Card';
import { CountContext } from '../../context/countContext';
import { useSelector } from 'react-redux';
import CheckoutForm from '../CheckoutForm/CheckoutForm';

export default function Cart() {
    const [cartItems, setCartItems] = useState([]);
    const { cartCount } = useContext(CountContext);
    const itemData = useSelector(state => state.products);

    const updateCart = () => {
        const cartItemsFromStorage = JSON.parse(localStorage.getItem('cart')) || [];
        setCartItems(cartItemsFromStorage);
    }

    useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cartItemsFromStorage);
    }, [cartCount]);

    return (
        <>
            {cartItems.length > 0 ? (
                <CheckoutForm updateCart={updateCart} />
            ) : (
                <section className="empty-cart">
                    <h2>Sorry, the cart is empty</h2>
                </section>
            )}
            <section className="cartPages">
                <h2>Cart</h2>
                <div className="cartItems item-container">
                    {cartItems.length > 0 ? (
                        cartItems.map((sku) => {
                            const cartItemData = itemData.products.find((item) => item.sku === sku);
                            if (cartItemData) {
                                return <Card key={sku} {...cartItemData} inCart />;
                            }
                            return null;
                        })
                    ) : (
                        <p>There are no products in the cart</p>
                    )}
                </div>
            </section>
        </>
    );
}