import { useState, useEffect, useContext } from 'react';
import Card from '../Cards/Card';
import { CountContext } from '../../context/countContext';
import { useSelector } from 'react-redux';

export default function Fav() {
    const [favoriteItems, setFavoriteItems] = useState([]);
    const {favoriteCount} = useContext(CountContext);
    const itemData = useSelector(state => state.products);

    useEffect(() => {
        const favoriteItemsFromStorage = JSON.parse(localStorage.getItem('favorites')) || [];
        setFavoriteItems(favoriteItemsFromStorage);
    }, [favoriteCount]);

    return (
        <section className="favoritePages">
        <h2>Your Favorites</h2>
        <div className="favoriteItems item-container">
            {favoriteItems.map((sku) => {
            const favoriteItemData = itemData.products.find((item) => item.sku === sku);
            if (favoriteItemData) {
                return <Card key={sku} {...favoriteItemData} inFavorite />;
            }
            return null; 
            })}
        </div>
        </section>
    );
}