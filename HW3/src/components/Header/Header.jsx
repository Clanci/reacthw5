import Nav from '../Nav/Nav';
import NavItem from '../Nav/NavItem';
import { useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import { CountContext } from '../../context/countContext';
import imageCart from '../../assets/cart.png';
import imageStar from '../../assets/star2.png';

export default function Header(){
    const navigate = useNavigate();
    const { cartCount, favoriteCount } = useContext(CountContext);
    return(
        <header className="header">
            <div className="header_logo" onClick={() => navigate('/')} >TOP</div>
                <Nav>
                    <NavItem to="/">Home</NavItem>
                    <NavItem to="/cart">Cart</NavItem>
                    <NavItem to="/favorites">Favorites</NavItem>
                </Nav>
            <div className='header_info'>
                <div className="cart">
                    <p>{cartCount}</p>
                    <img src={imageCart} alt="#" />
                </div>
                <div className="favorite">
                    <p>{favoriteCount}</p>
                    <img src={imageStar} alt="#" />
                </div>
            </div>
        </header>
    );
}