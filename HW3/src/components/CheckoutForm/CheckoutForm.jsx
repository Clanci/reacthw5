import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';
import Modal from '../Modal/Modal';
import ModalClose from '../Modal/ModalClose';
import ModalHeader from '../Modal/ModalHeader';
import ModalBody from '../Modal/ModalBody';
import ModalFooter from '../Modal/ModalFooter';

const CheckoutForm = ({updateCart}) => {
    const [formData, setFormData] = useState(null);
    const [isModalVisible, setModalVisible] = useState(false);
    const openModal = () => {
        setModalVisible(true);
    }
    const closeModal = () => {
        setModalVisible(false);
    }
    const confirmBuy = () => {
        console.log(formData); 
        closeModal();
        const purchasedItems = JSON.parse(localStorage.getItem('cart')) || [];
        console.log("Buyed products:", purchasedItems);
        localStorage.removeItem('cart');
        updateCart();
    }

    return (
        <>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    address: '',
                    phoneNumber: ''
                }}
                validationSchema={Yup.object({
                    firstName: Yup.string().required('*Required'),
                    lastName: Yup.string().required('*Required'),
                    age: Yup.number().required('*Required').positive('Age must be positive number').integer('Age must be an integer').min(18, 'Must be 18 or older'),
                    address: Yup.string().required('*Required'),
                    phoneNumber: Yup.string().required('*Required').matches(/^\d{10}$/, 'Mobile phone number must consist of 10 digits')
                })}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    setTimeout(() => {
                        setFormData(values);
                        openModal();
                        setSubmitting(false);
                        resetForm();
                    }, 200);
                }}
            >
            <Form className="form-container">
                <div>
                    <label htmlFor="firstName">Name:</label>
                    <Field type="text" id="firstName" name="firstName" />
                    <ErrorMessage name="firstName" component="div" className="error" />
                </div>
                <div>
                    <label htmlFor="lastName">Last Name:</label>
                    <Field type="text" id="lastName" name="lastName" />
                    <ErrorMessage name="lastName" component="div" className="error" />
                </div>
                <div>
                    <label htmlFor="age">Age:</label>
                    <Field type="number" id="age" name="age" />
                    <ErrorMessage name="age" component="div" className="error" />
                </div>
                <div>
                    <label htmlFor="address">Delivery address:</label>
                    <Field type="text" id="address" name="address" />
                    <ErrorMessage name="address" component="div" className="error" />
                </div>
                <div>
                    <label htmlFor="phoneNumber">Phone Number:</label>
                    <Field type="tel" id="phoneNumber" name="phoneNumber" />
                    <ErrorMessage name="phoneNumber" component="div" className="error" />
                </div>
                <button type="submit">Checkout</button>
            </Form>
        </Formik>

        {isModalVisible && (
                <Modal
                classNames={"BuyModal"}
                onClose={closeModal}
                isVisible={isModalVisible}
                >
                    <ModalClose onClick={closeModal} />
                    <ModalHeader>Purchase Confirmation</ModalHeader>
                    <ModalBody>
                        <div>
                            <p>Name: {formData.firstName}</p>
                            <p>Last Name: {formData.lastName}</p>
                            <p>Age: {formData.age}</p>
                            <p>Delivery address: {formData.address}</p>
                            <p>Phone Number: {formData.phoneNumber}</p>
                        </div>
                    </ModalBody>
                    <ModalFooter
                        firstText={"Yes"}
                        firstClick={confirmBuy}
                        secondaryText={"No"}
                        secondaryClick={closeModal}>
                    </ModalFooter>
                </Modal>
            )}
        </>
    );
}

export default CheckoutForm;

import PropTypes from 'prop-types';
CheckoutForm.propTypes = {
    updateCart: PropTypes.func
};